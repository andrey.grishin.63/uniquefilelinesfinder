# UniqueFileLinesFinder

реализация https://github.com/Ecwid/new-job/blob/master/IP-Addr-Counter.md

Программа для поиска уникальных значений ip-адресов в файле вида:\
145.67.23.4\
8.34.5.23\
89.54.3.124\
89.54.3.124\
3.45.71.5\
...

Файл делится на батчи по n-строк. Каждый батч обрабатывается последовательно:
сортируются строки, пишутся в новый файл.

После обработки всего файла он разбит на отсортированные части. Далее эти n-части(файлы) читаются одновременно n-ридерами по одной
строке и среди них ищется минимальных элемент. Если он больше предыдущего - делается инкремент числа уникальных ip и читается
следующая строка этого ридера. Повтораяется пока остаются непрочитанные строки в файлах.

Параметры для запуска:
args[0] - название файла с ip-адресами для подсчета уникальных.
