package utils

import model.SortedBatchResult
import java.io.BufferedOutputStream
import java.io.DataOutputStream
import java.io.File
import java.io.FileOutputStream

object FileUtils {

    fun createFromLinesOfLong(fileName: String, lines: SortedBatchResult): File {
        val start = System.currentTimeMillis()
        val file = File(fileName)

        val dataOutputStream = DataOutputStream(BufferedOutputStream(FileOutputStream(file)))
        //Массив с лишними 0 отсортирован в порядке возрастания, поэтому бежим начиная с size - realCount
        for (i in lines.result.size - lines.realCount until lines.result.size) {
            dataOutputStream.writeLong(lines.result[i])
        }

        dataOutputStream.flush()
        dataOutputStream.close()
        println("Файл создан $fileName ${System.currentTimeMillis() - start}")
        return file
    }
}