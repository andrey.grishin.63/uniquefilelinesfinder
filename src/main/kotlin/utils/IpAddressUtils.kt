package utils

import kotlin.math.pow

object IpAddressUtils {

    /**
     * @param arr - разряды ip-адреса
     * @return long-представление ip-адреса
     */
    fun toLong(arr: IntArray): Long {
        var result: Long = 0

        arr.forEachIndexed { idx, ipPart ->
            val power = 3 - idx
            result += (ipPart * 256.0.pow(power)).toLong()
        }

        return result
    }
}