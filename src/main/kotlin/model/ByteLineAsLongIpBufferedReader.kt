package model

import java.io.Closeable
import java.io.File
import java.io.Reader

/**
 * BufferedReader читает по строчкам, лишние преобразования в стрингу и забивает память которую надо чистить
 * Поэтому преобразовываем сразу в long представление ip.
 */
class ByteLineAsLongIpBufferedReader(file: File) : Closeable {

    private val reader: Reader

    init {
        reader = file.bufferedReader()
    }

    fun ready(): Boolean {
        return reader.ready()
    }

    fun nextLines(count: Int): SortedBatchResult {
        val startRead = System.currentTimeMillis()

        val decimalIpAddresses = LongArray(count)

        var realCount = 0
        for (i in 0 until count) {
            val ipAddress = nextLineAsLongIpAddress()

            if (ipAddress == -1L) break

            decimalIpAddresses[i] = ipAddress
            realCount++
        }

        println("Строки прочитаны ${System.currentTimeMillis() - startRead}")

        return SortedBatchResult(decimalIpAddresses, realCount)
    }

    /**
     * Читаем символы из файла
     * разбиваем построчно и по разделителям разрядов ip-адреса('.')
     * конвертируем символы разрядов в int, потом все разряды складываем в long-представления ip-адреса
     *
     * @return long-представления ip-адреса
     */
    private fun nextLineAsLongIpAddress(): Long {
        if (!reader.ready()) return -1

        var read = reader.read()

        if (read == -1) return -1

        if (isLineSep(read)) {
            read = reader.read()
        }

        val buffer = IntArray(4)
        for (j in 0..3) {
            val block = intArrayOf(48, 48, 48)

            for (k in 0..2) {
                if (isDot(read)) {
                    reorganizeArray(block, k)
                    break
                }
                if (isLineSep(read)) {
                    reorganizeArray(block, k)
                    break
                }
                if (read == -1) {
                    reorganizeArray(block, k)
                    break
                }

                block[k] = read
                read = reader.read()
            }

            buffer[j] = convert(block)

            if (isDot(read)) {
                read = reader.read()
            }
        }
        return convertIntArrToInt(buffer)
    }

    private fun convertIntArrToInt(arr: IntArray): Long {
        var ip: Long = 0
        for (part in arr) {
            ip = ip * 1000 + part
        }
        return ip
    }

    /**
     * Сдвигаем элементы массива в конец, если прочитали не все три символа для текущего разряда.
     *
     * @param arr       символы разряда
     * @param countNot0 количество прочитанных символов разряда из трёх необходимых
     */
    private fun reorganizeArray(arr: IntArray, countNot0: Int) {
        if (countNot0 == 1) {
            arr[2] = arr[0]
            arr[1] = 48
            arr[0] = 48
        }
        if (countNot0 == 2) {
            arr[2] = arr[1]
            arr[1] = arr[0]
            arr[0] = 48
        }
    }

    private fun isDot(c: Int): Boolean {
        return c == '.'.code
    }

    private fun isLineSep(c: Int): Boolean {
        return c == '\n'.code //todo process System.lineSeparator() if use on windows
    }

    /**
     * Конвертируем массив разрядов ip-адреса в long-представления ip-адреса
     *
     * @param arr значения разрядов ip-адреса
     * @return long-представления ip-адреса
     */
    private fun convert(arr: IntArray): Int {
        var result = 0
        for (element in arr) {
            val digit: Int = element - '0'.code
            result *= 10
            result += digit
        }
        return result
    }

    override fun close() {
        reader.close()
    }
}