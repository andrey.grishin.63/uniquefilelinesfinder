package model

import java.util.*

/**
 * Класс нужен в основном для хранения реального количества записей
 * - издержки из-за объявления массива конкретной размерности
 */
class SortedBatchResult(val result: LongArray, val realCount: Int) {

    init {
        val startSort = System.currentTimeMillis()
        Arrays.parallelSort(result)
        println("Соритровка ${System.currentTimeMillis() - startSort}")
    }
}