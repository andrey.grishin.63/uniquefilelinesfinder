package model

import java.io.DataInputStream
import java.io.EOFException

/**
 * Не наследуем итератор чтобы не работать с объектами и избежать лишких автобоксингов
 */
class IteratorWithState(private val stream: DataInputStream) : Comparable<IteratorWithState> {
    /** текущий ip(long) в файле*/
    var value: Long = 0
        private set

    init {
        moveToNextValue()
    }

    /**
     * Переходим к следующей строчке в файле
     * если нету следующей строки - проставляем состояние -1
     */
    fun moveToNextValue() {
        value = try {
            stream.readLong()
        } catch (e: EOFException) {
            -1
        }
    }

    override fun compareTo(other: IteratorWithState): Int {
        return value.compareTo(other.value)
    }

    fun close() {
        stream.close()
    }
}