import utils.FileUtils.createFromLinesOfLong
import model.ByteLineAsLongIpBufferedReader
import model.SortedBatchResult
import java.io.File
import java.util.function.Consumer

class FilesOfSortedPartUniqueFinder(fileName: String, private val partSize: Int) {

    private val file: File = File(fileName)

    /**
     * Делим файл на отсортированные по значениям файлы
     * Бежим по промежуточным файлам, считаем количество уникальных
     *
     * @return количество уникальный ip-адресов в файле
     */
    fun sort(): Long {
        val start = System.currentTimeMillis()
        val filesWithSortedPart: MutableList<File> = ArrayList()

        val byteLineAsLongIpBufferedReader = ByteLineAsLongIpBufferedReader(file)
        byteLineAsLongIpBufferedReader.use { reader ->
            var iterationNumber = 0
            while (reader.ready()) { //Проверяем готовность ридера обрабатывать следующий part
                iterationNumber++

                //Получаем следующий батч строк переведенных в long представление ip-адреса
                val decimalIpAddresses = reader.nextLines(partSize)

                //Пишем отсортированную часть в новый файл
                val resultFile = createFileForSortedPart(iterationNumber, decimalIpAddresses)
                filesWithSortedPart.add(resultFile)
            }
        }
        byteLineAsLongIpBufferedReader.close()
        println(
            "Создано файлов: ${filesWithSortedPart.size}. Затраченное время: ${System.currentTimeMillis() - start}."
        )

        val uniqueCount = findUniqueInFiles(filesWithSortedPart)
        filesWithSortedPart.forEach(Consumer(File::delete))
        return uniqueCount
    }

    private fun createFileForSortedPart(partNumber: Int, decimalIpAddresses: SortedBatchResult): File {
        val fileName = "part$partNumber"
        return createFromLinesOfLong(fileName, decimalIpAddresses)
    }

    private fun findUniqueInFiles(files: List<File>): Long {
        SortedFileOfDecimalLineUniqueCounter(files).use { return it.countUniqueLines() }
    }
}