fun main(args: Array<String>) {
    try {
        val start = System.currentTimeMillis()

        val fileName = args[0]
        val filePartLineSize =
            (Runtime.getRuntime().totalMemory() / (1024 * 1024)).toInt()* 39000 //размер батча, вычислено опытным путем:)

        val uniqueFinder = FilesOfSortedPartUniqueFinder(fileName, filePartLineSize)
        val count = uniqueFinder.sort()

        println(
            "Результат: $count уникальных ip-адресов. Общее затраченное время: ${System.currentTimeMillis() - start}."
        )
    } catch (e: Exception) {
        System.err.println("Не удалось выполнить программу! Причина: ${e.message}")
        throw RuntimeException(e)
    }
}