import model.IteratorWithState
import java.io.BufferedInputStream
import java.io.Closeable
import java.io.DataInputStream
import java.io.File
import java.io.FileInputStream
import java.util.*
import java.util.stream.Collectors

class SortedFileOfDecimalLineUniqueCounter(files: List<File>) : Closeable {

    private val iterators: Queue<IteratorWithState>

    private val readers: List<DataInputStream> = files.map { file: File -> DataInputStream(BufferedInputStream(FileInputStream(file))) }.toList()

    init {
        //Преобразуем ридеры в очередь с приоритетом (наименьшее текущее значение - наивысший приоритет)
        iterators = readers.stream()
            .map { reader: DataInputStream -> IteratorWithState(reader) }
            .collect(Collectors.toCollection { PriorityQueue() })
    }

    /**
     * Бежим по итераторам отсортированных частей в файлах
     * Ищем наименьший, сравниваем с последним найденным, делаем count++ если предыдущий меньше
     *
     * @return количество уникальных строк во всех полученных в конструкторе файлах
     */
    fun countUniqueLines(): Long {
        val start = System.currentTimeMillis()

        var count: Long = 0
        var lastValue: Long = -1

        var smallestItem: IteratorWithState?
        while (iterators.poll().also { smallestItem = it } != null) {
            val currentValue = smallestItem!!.value

            if (currentValue != -1L) {
                if (lastValue < currentValue) {
                    count++
                }
                lastValue = currentValue
                smallestItem!!.moveToNextValue()
                iterators.add(smallestItem)
            }
        }

        for (iterator in iterators) {
            iterator.close()
        }

        println(
            "Подсчитано количество уникальных строк в файлах: $count." +
                    " Затраченное время: ${System.currentTimeMillis() - start}."
        )
        return count
    }

    override fun close() {
        for (reader in readers) {
            reader.close()
        }
    }


}